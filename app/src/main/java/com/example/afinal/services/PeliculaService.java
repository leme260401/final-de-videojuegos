package com.example.afinal.services;

import com.example.afinal.entities.Pelicula;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PeliculaService {

    @GET("peliculas/N000170218")
    Call<List<Pelicula>> getPeliculas();

    @POST("peliculas/N000170218")
    Call<Pelicula> postPelicula(@Body Pelicula pelicula);

}
