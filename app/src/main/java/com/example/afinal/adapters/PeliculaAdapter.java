package com.example.afinal.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.example.afinal.R;
import com.example.afinal.entities.Pelicula;

import org.w3c.dom.Text;

import java.util.List;

public class PeliculaAdapter extends RecyclerView.Adapter<PeliculaAdapter.PeliculaViewHolder> {

    List<Pelicula> peliculas;

    public PeliculaAdapter(List<Pelicula> peliculas) {this.peliculas = peliculas;}

    @Override
    public PeliculaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pelicula, parent, false);
        return new PeliculaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeliculaViewHolder holder, int position) {
        View view = holder.itemView;
        Pelicula pelicula = peliculas.get(position);

        ImageView ivPelicula = view.findViewById(R.id.imgListPeliculas);
        TextView tvNombreList = view.findViewById(R.id.tvListPeliculas);

        tvNombreList.setText(pelicula.nombre);
        Glide.with(view).asBitmap().load(pelicula.imagen).override(200,200).transform(new CircleCrop()).into(ivPelicula);

    }

    @Override
    public int getItemCount() {
        return peliculas.size();
    }

    public static class PeliculaViewHolder extends RecyclerView.ViewHolder {public PeliculaViewHolder(View itemView) {super(itemView);}}

}
