package com.example.afinal;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.afinal.entities.Pelicula;
import com.example.afinal.services.PeliculaService;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FormPeliculaActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pelicula);

        EditText edtNombre = findViewById(R.id.edtNombre);
        EditText edtVistas = findViewById(R.id.edtVistas);
        EditText edtFechaEstreno = findViewById(R.id.edtFechaEstreno);
        EditText edtTienda1 = findViewById(R.id.edtTienda1);
        EditText edtTienda2 = findViewById(R.id.edtTienda2);
        EditText edtTienda3 = findViewById(R.id.edtTienda3);
        EditText imgForm = findViewById(R.id.edtImagen);
        Button btnRegistrar = findViewById(R.id.btnRegistrar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PeliculaService service = retrofit.create(PeliculaService.class);

        btnRegistrar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String nombre = edtNombre.getText().toString();
                int vistas = Integer.parseInt(edtVistas.getText().toString());
                String fechaEstreno = edtFechaEstreno.getText().toString();
                String tienda1 = edtTienda1.getText().toString();
                String tienda2 = edtTienda2.getText().toString();
                String tienda3 = edtTienda3.getText().toString();
                String imagen = imgForm.getText().toString();

                Pelicula pelicula = new Pelicula(nombre, vistas, fechaEstreno, tienda1, tienda2, tienda3, imagen);

                Call<Pelicula> peliculaCall = service.postPelicula(pelicula);
                peliculaCall.enqueue(new Callback<Pelicula>() {
                    @Override
                    public void onResponse(Call<Pelicula> call, Response<Pelicula> response) {

                    }

                    @Override
                    public void onFailure(Call<Pelicula> call, Throwable t) {

                    }
                });

                Intent intent = new Intent(FormPeliculaActivity.this, ListPeliculasActivity.class);
                startActivity(intent);

            }
        });

    }
}