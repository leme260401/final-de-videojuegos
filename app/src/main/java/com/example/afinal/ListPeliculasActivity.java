package com.example.afinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.afinal.adapters.PeliculaAdapter;
import com.example.afinal.entities.Pelicula;
import com.example.afinal.services.PeliculaService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListPeliculasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_peliculas);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PeliculaService service = retrofit.create(PeliculaService.class);
        Call<List<Pelicula>> peliculas = service.getPeliculas();

        peliculas.enqueue(new Callback<List<Pelicula>>() {
            @Override
            public void onResponse(Call<List<Pelicula>> call, Response<List<Pelicula>> response) {
                List<Pelicula> p = response.body();
                RecyclerView rv = findViewById(R.id.rvPeliculas);
                rv.setLayoutManager(new LinearLayoutManager(ListPeliculasActivity.this));
                PeliculaAdapter adapter = new PeliculaAdapter(p);
                rv.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Pelicula>> call, Throwable t) {

            }
        });

    }
}