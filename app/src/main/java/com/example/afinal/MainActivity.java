package com.example.afinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMainRegistrar = findViewById(R.id.btnMainRegistrar);
        Button btnMainListar = findViewById(R.id.btnMainListar);
        Button btnSincronizar = findViewById(R.id.btnSincronizar);

        btnMainRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FormPeliculaActivity.class);
                startActivity(intent);
            }
        });

        btnMainListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListPeliculasActivity.class);
                startActivity(intent);
            }
        });

        btnSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConexionSQLiteHelper con = new ConexionSQLiteHelper(MainActivity.this, "bd_peliculas", null, 1);
            }
        });

    }
}