package com.example.afinal.utilidades;

public class Utilidades {

    public static final String TABLA_PELICULA="pelicula";
    public static final String CAMPO_NOMBRE="nombre";
    public static final String CAMPO_VISTAS="vistas";
    public static final String CAMPO_FECHAESTRENO="fecha_de_estreno";
    public static final String CAMPO_TIENDA1="tienda_1";
    public static final String CAMPO_TIENDA2="tienda_2";
    public static final String CAMPO_TIENDA3="tienda_3";
    public static final String CAMPO_IMAGEN="imagen";

    public static final String CREAR_TABLA_PELICULA = "CREATE TABLE "
            +TABLA_PELICULA+" ("+
            CAMPO_NOMBRE+" TEXT, "+
            CAMPO_VISTAS+ " INTEGER, "+
            CAMPO_FECHAESTRENO+" TEXT, "+
            CAMPO_TIENDA1+" TEXT, "+
            CAMPO_TIENDA2+" TEXT, "+
            CAMPO_TIENDA3+" TEXT, "+
            CAMPO_IMAGEN+" TEXT)";

}
